## Final Project Bootcamp

## Kelompok 5

## Anggota Kelompok

-   Yofrizal Fadli - 2057201011 (@muhammadalakbar17)
-   Yoga Ifantri - 2057201078 (@yoga21ifantri)
-   Dwi Anggrai Ningsih - 2057201035 (@anggraidn)
-   Sasmitha - 2057201004 (@mitha291)

## Tema Project

Forum Tanya Jawab (Kumpul-Gamers)

## Deskripsi Singkat

- Disini kami membuat Forum Tanya Jawab (Kumpul-Gamers)
- Dimana untuk mengakses Fitur Utama, User harus login terlebih dahulu, jika tidak user hanya dapat melihat halaman utama saja.
- Setelah login, User akan mendapatkan akses ke Fitur (Menu kategori, Tanya, Jawab, Profil dan UI Badge dengan menampilkan total jumlah jawaban)
- User dapat membuat Pertanyaan yang nantinya akan dapat dijawab oleh user lain, user hanya dapat menghapus pertanyaan dan jawaban milik sendiri.
- User dapat membuat Kategori dan user hanya dapat menghapus kategori milik sendiri.

## Alur

- 1 user memiliki 1 profil, 1 profil pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak kategori, 1 kategori pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak pertanyaan, 1 pertanyaan pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak jawaban, 1 jawaban pasti dimiliki oleh 1 user
- 1 kategori bisa memiliki banyak pertanyaan, 1 pertanyaan pasti memiliki 1 kategori
- 1 pertanyaan bisa memiliki banyak jawaban, 1 jawaban pasti memiliki 1 pertanyaan

## ERD

<img src="kumpul-gamers/public/images/ERD_kumpul_gamers.png">

## Link Video Demo Aplikasi :

-   Yofrizal Fadli : [https://drive.google.com/file/d/1yVAbDLkMK0FlVuHOpbZMH2ErTMi9METG/view?usp=sharing]
-   Yoga Ifantri : [https://drive.google.com/file/d/1UeHOxrar3bEDvSyA0yxxZVG8wNmJQvgm/view?usp=sharing]
-   Dwi Anggrai Ningsih : [https://drive.google.com/file/d/1sHabeptHQPStCcP8-NIH7Nb-ZQ98iaKG/view?usp=share_link]
-   Sasmitha : []

## Link Deploy : [http://kumpul-gamers.herokuapp.com/].

## Template

-   Landing Page : [https://themewagon.com/themes/free-bootstrap-5-html-css-nonprofit-website-template-chariteam/]
-   Dashboard : [https://themewagon.com/themes/free-bootstrap-4-html-5-admin-dashboard-website-template-skydash/]

## Package

- DataTables
- TinyMCE
- SweetAlert
- Laravel PDF
