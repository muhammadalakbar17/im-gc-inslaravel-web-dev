<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    // use HasFactory;
    protected $table = 'answers';
    protected $fillable = ['answer', 'image', 'users_id', 'questions_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class, 'questions_id');
    }
}
