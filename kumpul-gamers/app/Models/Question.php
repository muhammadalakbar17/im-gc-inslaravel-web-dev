<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // use HasFactory;
    protected $table = 'questions';
    protected $fillable = ['title', 'content', 'image', 'users_id', 'categories_id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function answer()
    {
        return $this->hasMany(Answer::class, 'questions_id');
    }
}
