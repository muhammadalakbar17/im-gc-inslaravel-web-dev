<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        $profile = Profile::where('users_id', $id)->first();
        return view('profiles.index', compact('profile'));
    }

    // public function create()
    // {
    //     //
    // }

    // public function store(Request $request)
    // {
    //     //
    // }

    // public function show(string $id)
    // {
    //     //
    // }

    // public function edit(string $id)
    // {
    //     //
    // }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'age' => 'required',
            'address' => 'required',
            'biodata' => 'required',
        ]);

        $profile = Profile::find($id);
        $profile->age = $request->age;
        $profile->address = $request->address;
        $profile->biodata = $request->biodata;
        $profile->save();
        
        Alert::success('Success', 'Profile has been successfully updated');
        return redirect('/profile');
    }

    // public function destroy(string $id)
    // {
    //     //
    // }
}
