<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::all();
        $title = 'Delete!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);
        return view('categories.index', compact('category'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'category_name' => 'required',
    	]);
 
        $id = Auth::id();
        $category = new Category;
        $category->category_name = $request->category_name;
        $category->users_id = $id;
        $category->save();

        Alert::success('Success', 'New category has been added successfully');
    	return redirect('/category');
    }

    public function show(string $id)
    {
        $category = Category::find($id);
        return view('categories.show', compact('category'));
    }

    public function edit(string $id)
    {
        $category = Category::find($id);
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'category_name' => 'required',
        ]);
        
        $category = Category::find($id);
        $category->category_name = $request->category_name;
        $category->update();

        Alert::success('Success', 'Category has been successfully updated');
        return redirect('/category');
    }

    public function destroy(string $id)
    {
        $category = Category::find($id);
        $category->delete();

        Alert::success('Success', 'Category has been successfully deleted');
        return redirect('/category');
    }

    public function pdf()
    {
        $category = Category::all();

        $pdf = Pdf::loadView('files_print.pdf', compact('category'));
        return $pdf->download('kumpul-gamers-'.Carbon::now()->timestamp.'.pdf');
    }
}
