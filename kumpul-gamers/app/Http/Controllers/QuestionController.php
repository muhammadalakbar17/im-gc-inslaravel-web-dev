<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Category;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class QuestionController extends Controller
{
    public function index()
    {
        $users = User::all();
        $question = Question::with(['category'])->latest()->get();
        $title = 'Delete!';
        $text = "Are you sure you want to delete?";
        confirmDelete($title, $text);
        return view('questions.index', compact('question', 'users'));
    }

    public function create()
    {
        $category = Category::all();
        
        if (count($category) < '1') {
            return view('categories.create');
        } else {
            return view('questions.create', compact('category'));
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'categories_id' => 'required',
            'image' => 'required|mimes:png,jpeg,jpg|max:5120',
        ]);

        $id = Auth::id();
        $imagename = time().'.'.$request->image->extension();
        $request->image->move(public_path('images/questions/'), $imagename);

        $question = new Question;
        $question->title = $request->title;
        $question->content = $request->content;
        $question->categories_id = $request->categories_id;
        $question->users_id = $id;
        $question->image = $imagename;
        $question->save();

        Alert::success('Success', 'New question has been successfully added');
        return redirect('/question');
    }

    public function show(string $id)
    {
        $question = Question::find($id);
        $category = Category::all();
        return view('questions.show', compact('question', 'category'));
    }

    public function edit(string $id)
    {
        $question = Question::find($id);
        $category = Category::all();
        return view('questions.edit', compact('question', 'category'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'categories_id' => 'required',
            'image' => 'mimes:png,jpeg,jpg|max:5120',
        ]);
        
        $question = Question::find($id);
        
        if($request->has('image')) {
            $path = "images/questions/";
            File::delete($path . $question->image);
            $imagename = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/questions/'), $imagename);
            $question->image = $imagename;
            $question->save();
        }

        $question->title = $request->title;
        $question->content = $request->content;
        $question->categories_id = $request->categories_id;
        $question->save();

        Alert::success('Success', 'Question has been updated successfully');
        return redirect('/question');
    }

    public function destroy(string $id)
    {
        $question = Question::find($id);
        $path = "images/questions/";
        File::delete($path . $question->image);
        $question->delete();

        Alert::success('Success', 'Question has been successfully deleted');
        return redirect('/question');
    }

    // public function isOwner()
    // {
    //     return Auth::user()->id == $this->users->id;
    // }
}
