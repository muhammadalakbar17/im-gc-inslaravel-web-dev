<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    
    // public function index()
    // {
    //     //
    // }

    // public function create()
    // {
    //     //
    // }

    public function store(Request $request)
    {
        $request->validate([
                'answer' => 'required',
                'image' => 'mimes:jpg,png,jpeg|max:5120',
            ]);

        $id = Auth::id();
        $answer = new Answer;

        if ($request->has('image')){
            $imagename = time().'.'.$request->image->extension();  
            $request->image->move(public_path('images/answers'), $imagename);
            $answer->image = $imagename;
        }

        $answer->answer = $request->answer;
        $answer->users_id = $id;
        $answer->questions_id = $request->questions_id;
        $answer->save();

        Alert::success('Success', 'New answer has been successfully added');
        return redirect('/question/'.$request->questions_id);
    }

    public function show(string $id)
    {
        $answer = Answer::find($id);
        return view('answers.edit', compact('answer'));
    }

    public function edit(string $id)
    {
        $answer = Answer::find($id);
        return view('answers.edit', compact('answer'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'answer' => 'required',
            'image' => 'mimes:jpg,png,jpeg|max:5120',
        ]);
        
        $users = Auth::id();
        $answer = Answer::find($id);
        
        if($request->has('image')) {
            $path = "images/answers/";
            File::delete($path . $answer->image);
            $imagename = time().'.'.$request->image->extension();
            $request->image->move(public_path('images/answers/'), $imagename);
            $answer->image = $imagename;
            $answer->save();
        }

        $answer->answer = $request->answer;
        $answer->users_id = $users;
        $answer->questions_id = $request->questions_id;
        $answer->save();

        Alert::success('Success', 'Answer has been updated successfully');
        return redirect('/question/'.$request->questions_id);
    }

    public function destroy(string $id)
    {
        $answer = Answer::find($id);
        $questions_id = $answer->questions_id;
        $path = "images/answers/";
        File::delete($path . $answer->image);
        $answer->delete();  

        Alert::success('Success', 'Answer has been successfully deleted');
        return redirect('/question/'.$questions_id);
    }
}
