<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\QuestionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {
    Route::resource('profile', ProfileController::class, ['only' => ['index', 'update']]);
    Route::resource('category', CategoryController::class);
    Route::resource('question', QuestionController::class, ['except' => ['show', 'index']]);
});

Route::resource('question', QuestionController::class, ['only' => ['show', 'index']]);

// Laravel UI Auth
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// 
Route::resource('answer', AnswerController::class);
Route::resource('category', CategoryController::class);

// Landing Page
Route::get('/about', function () {
    return view('pages_landing.about');
});

// PDF
Route::get('/pdf', [CategoryController::class, 'pdf']);