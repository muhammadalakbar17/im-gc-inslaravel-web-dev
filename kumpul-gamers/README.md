## Final Project Bootcamp

## Kelompok 5

## Anggota Kelompok

-   Yofrizal Fadli - 2057201011 (@muhammadalakbar17)
-   Yoga Ifantri - 2057201078 (@yoga21ifantri)
-   Dwi Anggrai Ningsih - 2057201035 (@anggraidn)
-   Sasmitha - 2057201004 (@mitha291)

## Tema Project

Forum Tanya Jawab (Kumpul-Gamers)

## Deskripsi Singkat

- Disini kami membuat Forum Tanya Jawab (Kumpul-Gamers)
- Dimana untuk mengakses Fitur Utama, User harus login terlebih dahulu, jika tidak user hanya dapat melihat halaman utama saja.
- Setelah login, User akan mendapatkan akses ke Fitur (Menu kategori, Tanya, Jawab, Profil dan UI Badge dengan menampilkan total jumlah jawaban)
- User dapat membuat Pertanyaan yang nantinya akan dapat dijawab oleh user lain, user hanya dapat menghapus pertanyaan dan jawaban milik sendiri.
- User dapat membuat Kategori dan user hanya dapat menghapus kategori milik sendiri.

## Alur

- 1 user memiliki 1 profil, 1 profil pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak kategori, 1 kategori pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak pertanyaan, 1 pertanyaan pasti dimiliki oleh 1 user
- 1 user dapat membuat banyak jawaban, 1 jawaban pasti dimiliki oleh 1 user
- 1 kategori bisa memiliki banyak pertanyaan, 1 pertanyaan pasti memiliki 1 kategori
- 1 pertanyaan bisa memiliki banyak jawaban, 1 jawaban pasti memiliki 1 pertanyaan

## ERD

<img src="kumpul-gamers/public/images/ERD_kumpul_gamers.png">

## Link Video Demo Aplikasi :

-   Yofrizal Fadli : [https://drive.google.com/file/d/1yVAbDLkMK0FlVuHOpbZMH2ErTMi9METG/view?usp=sharing]
-   Yoga Ifantri : [https://drive.google.com/file/d/1UeHOxrar3bEDvSyA0yxxZVG8wNmJQvgm/view?usp=sharing]
-   Dwi Anggrai Ningsih : [https://drive.google.com/file/d/1sHabeptHQPStCcP8-NIH7Nb-ZQ98iaKG/view?usp=share_link]
-   Sasmitha : []

## Link Deploy : [http://kumpul-gamers.herokuapp.com/].

## Template

-   Landing Page : [https://themewagon.com/themes/free-bootstrap-5-html-css-nonprofit-website-template-chariteam/]
-   Dashboard : [https://themewagon.com/themes/free-bootstrap-4-html-5-admin-dashboard-website-template-skydash/]

## Package

- DataTables
- TinyMCE
- SweetAlert
- Laravel PDF

<!-- <p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT). -->
