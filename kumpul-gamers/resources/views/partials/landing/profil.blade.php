<!-- Profil Start -->
<div class="container-xxl py-5">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
            <div class="d-inline-block rounded-pill bg-secondary text-primary py-1 px-3 mb-3">Kelompok 5</div>
            <h1 class="display-6 mb-5">Tugas Akhir Pelatihan Bootcamp di Garuda Cyber Institute</h1>
        </div>
        <div class="owl-carousel testimonial-carousel wow fadeInUp" data-wow-delay="0.1s">
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="{{asset('landing/img/profil-1.jpg')}}" style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <p>You don't have to bow your head. Just have a heart that cares for others.</p>
                    <h5 class="mb-1">Yofrizal Fadli - 2057201011</h5>
                    <span class="fst-italic">Mahasiswa SI-UNILAK</span>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="{{asset('landing/img/profil-2.jpg')}}" style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <p>Make any negative situation, turn it into positive situation.</p>
                    <h5 class="mb-1">Yoga Ifantri - 2057201078</h5>
                    <span class="fst-italic">Mahasiswa SI-UNILAK</span>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="{{asset('landing/img/profil-3.jpg')}}" style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <p>The accumulation of those little despairs is what makes a person an adult</p>
                    <h5 class="mb-1">Dwi Anggrainingsih - 2057201035</h5>
                    <span class="fst-italic">Mahasiswa SI-UNILAK</span>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="img-fluid bg-light rounded-circle p-2 mx-auto mb-4" src="{{asset('landing/img/profil-4.jpg')}}" style="width: 100px; height: 100px;">
                <div class="testimonial-text rounded text-center p-4">
                    <p>When you give up, that's when the game is over.</p>
                    <h5 class="mb-1">Sasmitha - 2057201003</h5>
                    <span class="fst-italic">Mahasiswa SI-UNILAK</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Profil End -->