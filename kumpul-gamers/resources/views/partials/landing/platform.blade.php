<!-- Platform Start -->
<div class="container-xxl py-5">
    <div class="container">
        <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
            <div class="d-inline-block rounded-pill bg-secondary text-primary py-1 px-3 mb-3">Platform</div>
            <h1 class="display-6 mb-5">Kamu Bermain Menggunakan Apa?</h1>
        </div>
        <div class="row g-4 justify-content-center">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="service-item bg-white text-center h-100 p-4 p-xl-5">
                    <img class="img-fluid mb-4" src="{{asset('landing/img/icon-1.png')}}" alt="">
                    <h4 class="mb-3">Android</h4>
                    <p class="mb-4">Game mobile sedang menjadi tren, karena bisa memainkannya di smartphone di mana pun kamu berada</p>
                    <a class="btn btn-outline-primary px-3" href="">
                        Selengkapnya
                        <div class="d-inline-flex btn-sm-square bg-primary text-white rounded-circle ms-2">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="service-item bg-white text-center h-100 p-4 p-xl-5">
                    <img class="img-fluid mb-4" src="{{asset('landing/img/icon-2.png')}}" alt="">
                    <h4 class="mb-3">PC</h4>
                    <p class="mb-4">Dengan menggunakan keyboard dan mouse sebagai kontrol, kamu dapat merasakan pengalaman bermain game yang lebih optimal</p>
                    <a class="btn btn-outline-primary px-3" href="">
                        Selengkapnya
                        <div class="d-inline-flex btn-sm-square bg-primary text-white rounded-circle ms-2">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="service-item bg-white text-center h-100 p-4 p-xl-5">
                    <img class="img-fluid mb-4" src="{{asset('landing/img/icon-3.png')}}" alt="">
                    <h4 class="mb-3">Console</h4>
                    <p class="mb-4">Console dirancang untuk kenyamanan dan kepraktisan dalam bermain game, serta menampilkan grafis yang memukau pada layar TV</p>
                    <a class="btn btn-outline-primary px-3" href="">
                        Selengkapnya
                        <div class="d-inline-flex btn-sm-square bg-primary text-white rounded-circle ms-2">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Platform End -->