<!-- partial:partials/_navbar.html -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
      <a class="navbar-brand brand-logo mr-5" href="/"><img src="{{asset('dashboard/images/logo-kumpul-gamers.svg')}}" class="ml-3" alt="logo"/></a>
      <a class="navbar-brand brand-logo-mini" href="/"><img src="{{asset('dashboard/images/logo-kumpul-gamers-mini.svg')}}" alt="logo"/></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="icon-menu"></span>
      </button>
      <ul class="navbar-nav navbar-nav-right">

      @auth
          <li class="nav-item nav-profile dropdown">
            <label class="font-weight-bold mr-3 mt-2">{{Auth::user()->name}}</label>
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="{{asset('dashboard/images/faces/man.png')}}" alt="profile"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="/profile">
                <i class="ti-settings text-primary"></i>
                Profile
              </a>
              <a class="dropdown-item" href="/logout"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="ti-power-off text-primary"></i>
                {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
            </div>
          </li>
      </div>
      @endauth

  </nav>
  <!-- partial -->