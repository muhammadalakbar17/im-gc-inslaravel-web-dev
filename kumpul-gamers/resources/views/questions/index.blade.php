@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Main Page</h4>
            <p class="card-description">
                Be a comfortable and healthy gaming community, stay united.
            <p>
            <a href="/question/create" class="btn btn-primary">Ingin menanyakan sesuatu?</a>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-3">
                @forelse ($question as $item)
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="card border shadow mb-3">
                                    <div class="card">
                                        <img src="{{asset('images/questions/'. $item->image)}}" style="height: auto" class="card-img-top" alt="question-image">
                                        <div class="card-img-overlay">
                                            @auth
                                            <span><a href="/category/{{$item->category->id}}" class="badge badge-danger">{{$item->category->category_name}}</a></span>
                                            <span class="badge badge-danger visually-hidden">{{$item->answer->count()}}</span>
                                            @endauth
                                        </div>
                                    </div>
                                    @auth
                                    <div class="card-header">
                                        <small class="card-description">Written by {{$item->user->name}}</small>
                                        <p class="card-text">
                                            <small class="text-muted" style="float: right">Last updated {{$item->updated_at->diffForHumans()}}</small>
                                        </p>
                                    </div>
                                    @endauth
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            @guest
                                            {{$item->title}}
                                            @else
                                            <a href="/question/{{$item->id}}" style="text-decoration: none">{{$item->title}}</a>
                                            @endguest
                                        </h5>
                                    <p class="card-text">
                                        {!!$item->content!!}
                                    </p>
                                    </div>
                                    <div class="card-footer">
                                    @auth
                                    <p class="card-text">
                                        <small class="text-muted">{{$item->created_at}}</small>
                                    <p>
                                    @if (Auth::user()->id == $item->user->id)
                                        <div style="float: right">
                                        <form action="/question/{{$item->id}}" method="POST">
                                            <a href="/question/{{$item->id}}/edit" class="btn btn-outline-info">Edit</a>
                                            <a href="/question/{{$item->id}}" type="button" class="btn btn-outline-danger" data-confirm-delete="true">Delete</a>
                                            </form>
                                        </div>
                                    @endif
                                    @endauth
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-center">
                    <h3 class="mt-4">This page doesn't have any questions yet</h3>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection


