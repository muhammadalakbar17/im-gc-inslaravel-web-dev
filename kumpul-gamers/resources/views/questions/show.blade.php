@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Discussion Page</h4>
            <p class="card-description">
                Be a comfortable and healthy gaming community, stay united.
            <p>
            <a href="/question/create" class="btn btn-primary">Ingin menanyakan sesuatu?</a>
            <div class="row row-cols-1 row-cols-md-2 g-3 ">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="card border shadow mb-3">
                                    <div class="card">
                                      <img src="{{asset('images/questions/'. $question->image)}}" style="height: auto" class="card-img-top" alt="question-image">
                                      <div class="card-img-overlay">
                                          <span><a href="/category/{{$question->category->id}}" class="badge badge-danger">{{$question->category->category_name}}</a></span>
                                          <span class="badge badge-danger visually-hidden">{{$question->answer->count()}}</span>
                                        </div>
                                    </div>
                                    @auth
                                    <div class="card-header">
                                      <small class="card-description">Written by {{$question->user->name}}</small>
                                        <p class="card-text">
                                            <small class="text-muted" style="float: right">Last updated {{$question->updated_at->diffForHumans()}}</small>
                                        </p>
                                    </div>
                                    @endauth
                                    <div class="card-body">
                                      <h5 class="card-title"><a>{{$question->title}}</a></h5>
                                        <p class="card-text">
                                            {!!$question->content!!}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        @auth
                                        <p class="card-text">
                                          <small class="text-muted">{{$question->created_at}}</small>
                                        <p>
                                        @if (Auth::user()->id == $question->user->id)
                                            <div style="float: right">
                                            <form action="/question/{{$question->id}}" method="POST">
                                                <a href="/question/{{$question->id}}/edit" class="btn btn-outline-info">Edit</a>
                                                  @csrf
                                                 @method('DELETE')
                                                 <input type="submit" class="btn btn-outline-danger" value="Delete">
                                                </form>
                                            </div>
                                        @endif
                                        @endauth
                                    </div>
                                    
                                    {{-- Card-Answer --}}
                                    @forelse ($question->answer as $item)
                                    <div class="card border m-2">
                                        <div class="card-header mt-2">
                                          <h5 class="card-title">{{$item->user->name}}</h5>
                                              {!!Str::limit($item->answer, 100)!!}
                                              <p class="card-text">
                                                @auth
                                                  <small class="text-muted" style="float: right">Last updated {{$item->updated_at->diffForHumans()}}</small>
                                                @endauth
                                              </p>
                                          </div>
                                      <div class="card-body">
                                          @if ($item->image !== null)
                                          <img src="{{asset('images/answers/'. $item->image)}}" style="height: auto" class="card-img-top" alt="answer-image"><br>
                                          @endif
                                        </div>
                                      <div class="card-footer">
                                          @auth
                                          <p class="card-text">
                                            <small class="text-muted">{{$item->created_at}}</small>
                                          <p>
                                          @if (Auth::user()->id == $item->user->id)
                                              <div style="float: right">
                                              <form action="/answer/{{$item->id}}" method="POST">
                                                  <a href="/answer/{{$item->id}}/edit" class="btn btn-outline-info">Edit</a>
                                                  @csrf
                                                 @method('DELETE')
                                                 <input type="submit" class="btn btn-outline-danger" value="Delete">
                                              </form>
                                              </div>
                                          @endif
                                          @endauth
                                        </div>
                                      </div>
                                    @empty
                                    <div class="card-footer text-center">
                                      <h4 class="mt-4">This post doesn't have answers yet</h4>
                                    </div>
                                    @endforelse
                                    {{-- Card-Answer-End --}}
                                    

                                    {{-- Card-Insert --}}
                                    @auth
                                    <form action="/answer" method="POST" enctype="multipart/form-data" class="m-4">
                                        @csrf
                                      <input type="hidden" value="{{$question->id}}" name="questions_id" >
                                      <div class="form-group mt-3">
                                        <label for="answer">Jawaban</label>
                                        <textarea class="form-control" id="answer" name="answer" placeholder="Let's give an answer.."></textarea>
                                        @error('answer')
                                            <div class="alert alert-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                      </div>
                                      <div class="form-group">
                                        <label>Gambar</label>
                                        <div class="input-group col-xs-12">
                                          <input type="file" class="form-control" id="image" name="image" placeholder="Upload Image">
                                          <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                          </span>
                                        </div>
                                        @error('image')
                                            <div class="alert alert-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                      </div>
                                      <button type="submit" class="btn btn-primary mr-2">Tambahkan Jawaban</button>
                                    </form>
                                @endauth
                                {{-- Card-Insert-End --}}

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="/question" class="btn btn-outline-primary">Kembali</button></a>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/v1bcg04pmhcgby3j5k61li24w4hjmkdildgzkhw3cfi5y85z/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ]
    });
  </script>
@endpush