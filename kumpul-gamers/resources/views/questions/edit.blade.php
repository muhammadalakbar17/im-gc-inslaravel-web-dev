@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Update Question</h4>
        <p class="card-description">
            Halaman untuk memperbarui Pertanyaan
        <p>
        <form action="/question/{{$question->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
          <div class="form-group mt-3">
            <label for="title">Judul</label>
            <input type="text" class="form-control" id="title" name="title" value="{{old('title', $question->title)}}" placeholder="Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
          <div class="form-group">
            <label for="content">Kontent</label>
            <textarea class="form-control" id="content" name="content" placeholder="Let's ask something..">{{old('content', $question->content)}}</textarea>
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
          <div class="form-group">
            <label>Gambar</label>
            <div class="input-group col-xs-12">
              <input type="file" class="form-control" id="image" name="image" placeholder="Upload Image">
              <span class="input-group-append">
                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
              </span>
            </div>
            @error('image')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
          <div class="form-group">
            <label for="categories_id">Kategori</label>
              <select class="form-control" id="categories_id" name="categories_id" value="{{old('categories_id', $question->categories_id)}}">
                @forelse ($category as $item)
                @if ($item->id === $question->categories_id)
                    <option value="{{$item->id}}" selected>{{$item->category_name}}</option>
                @else 
                    <option value="{{$item->id}}">{{$item->category_name}}</option> 
                @endif   
                @empty
                    <option value="">-- category not available --</option>
                @endforelse
                @error('categories_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
              </select>
            </div>
          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
          <a href="/question/{{$question->id}}" class="btn btn-light">Kembali</button></a>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/v1bcg04pmhcgby3j5k61li24w4hjmkdildgzkhw3cfi5y85z/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ]
    });
  </script>
@endpush