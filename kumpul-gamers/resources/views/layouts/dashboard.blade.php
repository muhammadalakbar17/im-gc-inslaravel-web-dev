<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Kumpul-Gamers.com</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('dashboard/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{asset('dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('dashboard/js/select.dataTables.min.css')}}">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('dashboard/css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('dashboard/images//logo-kumpul-gamers-mini.svg')}}" />
  @stack('headers')
</head>
<body>
  <div class="container-scroller">

    <!-- partial:partials/.dashboard.navbar -->
    @include('partials.dashboard.navbar')
    <!-- partial -->

    <div class="container-fluid page-body-wrapper">

      <!-- partial:partials/.dashboard.sidebar -->
      @include('partials.dashboard.sidebar')
      <!-- partial -->

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="row">
                <div class="col-12 col-xl-8 mb-4 mb-xl-0">

                  {{-- @guest
                    <h3 class="font-weight-bold">Welcome in {{ config('app.name', 'Laravel') }}.com</h3>
                  @else
                    <h3 class="font-weight-bold">Welcome, {{ Auth::user()->name }}</h3>
                  @endguest
                  
                  <h6 class="font-weight-normal mb-0">All systems are running smoothly! You have <span class="text-primary">3 unread alerts!</span></h6> --}}
                </div>
                <!-- Content Start -->
                @include('sweetalert::alert')
                @yield('content')
                <!-- Content End -->
                </div>
              </div>
            </div>
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/.dashboard.footer -->
        @include('partials.dashboard.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>   
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('dashboard/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  {{-- <script src="{{asset('dashboard/vendors/chart.js/Chart.min.js')}}"></script> --}}
  <script src="{{asset('dashboard/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('dashboard/js/dataTables.select.min.js')}}"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('dashboard/js/off-canvas.js')}}"></script>
  <script src="{{asset('dashboard/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('dashboard/js/template.js')}}"></script>
  {{-- <script src="{{asset('dashboard/js/settings.js')}}"></script> --}}
  {{-- <script src="{{asset('dashboard/js/todolist.js')}}"></script> --}}
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('dashboard/js/dashboard.js')}}"></script>
  {{-- <script src="{{asset('dashboard/js/Chart.roundedBarCharts.js')}}')}}"></script> --}}
  <!-- End custom js for this page-->
  @stack('scripts')
</body>

</html>

