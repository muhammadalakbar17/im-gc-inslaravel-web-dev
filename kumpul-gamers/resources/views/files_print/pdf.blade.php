<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
#category {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#category td, #category th {
  border: 1px solid #ddd;
  padding: 8px;
}

#category tr:nth-child(even){background-color: #f2f2f2;}

#category tr:hover {background-color: #ddd;}

#category th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
<body>
<h4>Page to display all categories</h4>
  <table id="category">
      <thead>
        <tr>
          <th>#</th>
          <th>Categories</th>
          <th>Amount Questions</th>
        </tr>
      </thead>
      <tbody>
      @forelse ($category as $key=>$value)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$value->category_name}}</td>
          <td>{{$value->question->count()}}</td>
        </tr>
      @empty 
      @endforelse  
      </tbody>
    </table>
  <h6>Copyright © {{date("D M Y")}} .kumpul-gamers.com</h6>
</body>
</html>