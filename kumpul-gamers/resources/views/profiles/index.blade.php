@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Profile Page</h4>
        <p class="card-description">
            Halaman untuk menampilkan Profil User
        <p>
        <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('put')
          <div class="form-group mt-3">
            <label>Nama</label>
            <input type="text" class="form-control" disabled value="{{$profile->user->name}}">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" disabled value="{{$profile->user->email}}">
          </div>
          <div class="form-group">
            <label>Umur</label>
            <div class="input-group">
                <input type="number" class="form-control" name="age" placeholder="Umur" value="{{old('age', $profile->age)}}">
                <div class="input-group-prepend">
                    <span class="input-group-text">Tahun</span>
                </div>
                @error('age')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="address" class="form-control" rows="3" placeholder="Alamat">{{old('address', $profile->address)}}</textarea>
            @error('address')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="biodata" class="form-control" rows="6" placeholder="Biodata">{{old('biodata', $profile->biodata)}}</textarea>
            @error('biodata')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
          <a href="/question" class="btn btn-light">Kembali</button></a>
        </form>
      </div>
    </div>
  </div>
@endsection