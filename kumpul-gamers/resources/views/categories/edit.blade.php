@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Updating Categories</h4>
        <p class="card-description">
            Halaman untuk mengubah Kategori {{$category->category_name}}
        <p>
        <form action="/category/{{$category->id}}" method="POST">
            @csrf
            @method('PUT')
          <div class="form-group mt-3">
            <label for="category">Kategori</label>
            <input type="text" class="form-control" id="category_name" name="category_name" value="{{$category->category_name}}" placeholder="Category">
            @error('category_name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
          </div>
          <button type="submit" class="btn btn-primary mr-2">Simpan</button>
          <a href="/category" class="btn btn-light">Kembali</button></a>
        </form>
      </div>
    </div>
  </div>
@endsection