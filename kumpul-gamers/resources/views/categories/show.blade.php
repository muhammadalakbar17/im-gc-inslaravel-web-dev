@extends('layouts.dashboard')

@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Post Categories</h4>
            <p class="card-description">
                Halaman untuk menampilkan seluruh Kategori Post
            <p>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-3">
                @forelse ($category->question as $item)
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="card border shadow mb-3">
                                    <div class="card">
                                        <img src="{{asset('images/questions/'. $item->image)}}" style="height: auto" class="card-img-top" alt="question-image">
                                        <div class="card-img-overlay">
                                            <span><a class="badge badge-danger">{{$item->category->category_name}}</a></span>
                                            <span class="badge badge-danger visually-hidden">{{$item->answer->count()}}</span>
                                        </div>
                                    </div>
                                    <div class="card-header">
                                        <p class="card-text">
                                            <a class="card-description" style="text-decoration: none">Written by {{$item->user->name}}</a>
                                            <small class="text-muted" style="float: right">Updated {{$item->updated_at->diffForHumans()}}</small>
                                        </p>
                                    </div>
                                    <div class="card-body">
                                    <h5 class="card-title"><a href="/question/{{$item->id}}" style="text-decoration: none">{{$item->title}}</a></h5>
                                        <p class="card-text">
                                            {!!$item->content!!}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        @auth
                                        <p class="card-text">
                                          <small class="text-muted">{{$item->created_at}}</small>
                                        <p>
                                        @if (Auth::user()->id == $item->user->id)
                                            <div style="float: right">
                                            <form action="/question/{{$item->id}}" method="POST">
                                                <a href="/question/{{$item->id}}/edit" class="btn btn-outline-info">Edit</a>
                                                  @csrf
                                                 @method('DELETE')
                                                 <input type="submit" class="btn btn-outline-danger" value="Delete">
                                                </form>
                                            </div>
                                        @endif
                                        @endauth
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="text-center">
                    <h3 class="mt-4">This page doesn't have any questions yet</h3>
                </div>
                @endforelse
            </div>
        </div>
        <a href="/question" class="btn btn-outline-primary">Kembali</button></a>
    </div>
</div>
@endsection