@extends('layouts.dashboard')

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Categories Page</h4>
        <p class="card-description">
            Halaman untuk menampilkan seluruh Kategori
        </p>
        <a href="/category/create" class="btn btn-primary">Tambah Kategori</a>
          <a type="button" href="/pdf" style="float: right" class="btn btn-outline-info btn-icon-text">
            Print
            <i class="ti-printer btn-icon-append"></i> 
          </a>
        <div class="table-responsive pt-3">
          <table class="table table-bordered" id="category">
            <thead>
              <tr>
                <th>#</th>
                <th>Categories</th>
                <th>Amount Questions</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @forelse ($category as $key=>$value)
              <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->category_name}}</td>
                <td>{{$value->question->count()}}</td>
                <td>
                    @auth
                    @if (Auth::user()->id == $value->users_id)
                    <form action="/category/{{$value->id}}" method="POST">
                      
                    <a href="/category/{{$value->id}}" type="button" class="btn btn-outline-success mr-2">Show</a>
                    <a href="/category/{{$value->id}}/edit" type="button" class="btn btn-outline-info mr-2">Edit</a>
                    <a href="/category/{{$value->id}}" type="button" class="btn btn-outline-danger" data-confirm-delete="true">Delete</a>
                    @else
                    <a href="/category/{{$value->id}}" type="button" class="btn btn-outline-success mr-2">Show</a>
                    @endif
                    @endauth
                  </form>
                </td>
              </tr>
              @empty
                  <tr colspan="3">
                      <td>No data available</td>
                      <td>No data available</td>
                      <td>No data available</td>
                      <td>No action available</td>
                  </tr>  
              @endforelse  
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>             
@endsection

@push('headers')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css"/>
@endpush

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#category').DataTable();
        } );
    </script>
@endpush